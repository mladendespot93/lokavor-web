import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Helmet } from 'react-helmet';
import { Redirect, Switch } from 'react-router-dom';
import { AppState } from './store/rootReducer';

import Layout from './layouts/main';
import Login from './pages/login';
import Packages from './pages/subscription/packages';
import Registration from './pages/registration';
import ShopRegistration from './pages/subscription/shopRegistration';
import SubscriptionCheckout from './pages/subscription/checkout';
import Download from './pages/download';
import { verifyAccessTokenAction } from './store/auth/actions';
import SubscriptionCheckoutLokalac from './pages/subscription_lokalac/checkout';
import ShopRegistrationLokalac from './pages/subscription_lokalac/shopRegistration';
import PackagesLokalac from './pages/subscription_lokalac/packages';
import DownloadLokalac from './pages/download_lokalac';
import RegistrationLokalac from './pages/registration_lokalac';
import LoginLokalac from './pages/login_lokalac';
import LayoutLokalac from './layouts/main/lokalac';
import DownloadLokalacRegistration from './pages/download_lokalac_registration';
import TermsLokalac from './pages/terms_lokalac';
import TermsLokalacBih from './pages/terms_lokalac_bih';
import Payment from './pages/stripe';

const App: React.FC = () => {
  const dispatch = useDispatch();

  const { isLogin, shopOwner } = useSelector((state: AppState) => state.auth);
  const currentUrl = window.location.href;

  useEffect(() => {
    dispatch(verifyAccessTokenAction());
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <meta property="og:title" content="Lokalac" />
        <meta property="og:description" content="Lokalac" />
        <meta
          property="og:image"
          content="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAABHNCSVQICAgIfAhkiAAADz1JREFUeF7tXQtwVNUZPudukt0gmwR5hZDdaAVBIU/w0fqkaqEqIYBSH5hIquJ0bK3iWKx2pp36GB2t1mIRIUlDUYt1IJsIWp/Y4rsQAlUBWzEPELGYzUbIJtm9f7+zSZY8drN37969e2+mdyaz2b33/I/znf+c/zz+/3Km0UWvvZbu7ei4ijOaSYxlMaJJXHxyPinAguhLYvwg4+wwfj8kMXlncuroWn7ZZW0aiaAJGWraORuEzpI5n8ZlIT+bxEh8Ej65D9+/xH3oQviUDkpE+5mFfcizC3dpIQDqRv1FW7ZkdsrdSyDoAlD5vipKRK8zxjdZrdaNfN68b1TRiLEQHdzjkGXfCgBQjgq3qyT3NUB6WrJZ/8gnzDiskgbYq7i6XK7ZMqeVaDkLYQGSChKhivhgWX+RZHrIWlLyiUY0hyVDB3cVyn75F6iEq9AoLJrwJOomzp+D5TzCc4qi1iMqQMjlyupktAogLNRE+DBEiFi1ze9fwRctOhoPPgGL8PsehfJL4kG/lybUoCpJSr2bZ5+hWA/FgHS4XOUYHwQYqXFUoj/pVs74MmtxsUsrfkSUxJrq7yLOfgWLGKUV3eHp0FHOpLuZI7+Kc45OYPhLESBel+sZdG43RyIWj/toZQ+kFpfcFyttOnDAJlvcq6HwjbHSUlFeWMsDkqPw1wDFP1z5YQGhTZvGepMsL+Ohs1QIoWWRl6xc+hGfP/+4GqLU/PHJJHfVoVF9T0157crQJs7HL+UOR0c4mmEBARgTOi2Wd6DEFO0EUk8J3c0HNlvqJXzu3GPRUKGDDdPIJ78CPU6JplzcniXawTkr5s6iQ6F4hASENm/O6LRI21FgRtwEU0f4Tas9bS6fM8enpHjAMqjr74bTg+hN7rRfzvnUzsF6hATE66p5DYP3pUqU1v8Z+oOtuORnSvjKTfWb8FxcPUIlcoR6BmPKGouz6NaIgHhra+6AB/I7tYx0Kcelubb5818djhc11v8c3tTjusijkgkG+MXcUSAaTfAaYCEdmzefwiW+D9aRopKHLsXgOx622Tun8jlLvg3Z+pp3TCGS6nFvtC4CqWYCl5hbT+eOGcEVioGAuFxvY8C5UDV9PQsSW2VbsOCnoVjKTTvfhpWbQg80rmqLs/DGPj2CgHjrauZjTapWzzqNiReRTFyaklpcfKA/HWquvxhO/1sx0da3MNTg03l2wX7B9gQgta4d+F6krywxciO2Glbyk/5UMJBvw/eLYqSsa/H+VhIApKum5hxZ4u/rKoU2zLzW1FGZfUv41NJwLsnye9qQ1pMK+QBEjpibBACBZ/U0jGW5niJoxQvd0y2pCxasFfT8zfVPYLXodq1o60kHct/Bcwqf6AHEVeOGZ5WupwDa8aJtmJfMEQuH1LxLzH7Ha0dbT0rUIDmLCnhXbe25MiMTmnmwsnyYvdvZ1IzpJDPh6pr1wiZlynjeWVuzElurD5lVCyE3cXmOLe+UAqNPBCPVMbqrhRzjx4sYPxZHetjQ94ndlVzozIVCZYaWM4JwGA9/A0Bc/8JzRltEjLZen0kpcOaj0DnRFjTS81jf2igAEWvzNiMJpkKWN1MKHACEj1VR1kBFqEEAIkMiRTuHBpJ8sCh7U/Kdp0ILq4FljCwaUaMAJOI+b2RKCX6C89aUfMeYBEsRO3tiX40UQBgAib1CEk0hAIipJ4XBGiR0WV3m77LYPt5R62rEAOJMdOOIlT8A+QqATIyVTkLLi/12nLfajj2Q8xIqSOzMCW6v2Ds31SrvYLUDbi9m6lWYqd8Ye50klAIlFzjXj5CJoQn20BVgbc133jEilk666uryZZI1OUqvoN7i8wh2D62zcmaZfnGRLOMCE0IM7CJmw9QDoq14Acdu4RGoY87ldwzoUk7R7N79ENdj8FDujE/z1YEqLMS2oMQyYjaoOmtqckniu3WourixEBZCLfUFZu22uDUpk0/M/Sq4hoVuaxu+mNNt7LUQgTaOAL2Cpbm5cUM+DoSHHHIQPHAM6HLs9GyJA7/4k+wHCDU3zMPXl+PPVDMOCLiScVhu1r8FxUEH5WrewfHGBB/ZV6FoP0B6rKS+Bh8i7tHwV9iDcgErcbmmYUN0Dw48JBtek/4CEvkxqCf1/YTDDpNx6GEvvpv7KKlQqNPlugcTrAfNDIiQHeHNt2IFYrWR9QjsoTsLhTUHryEbU2hZ3FtX+y5unGtkZQbINshC+u7JjTtfhLUb8ryA4nCEQOtyuSYi2hbRU/w0U4ASBhA68vFo8naJI04zDabHNu4YPU9xwI4QXoQmMIv0Hiwl02DKhBRHzENC3UDXlQWj/wfcl+8YRI+PeCpC88ZPbw8lz/BBn3V1kztl/6uwlDMNokxoMcJYSHCQb/l0LMle0Vefn2A9XJyPu1ZV0GdQGZfLjkC4DWhhxQlWZlj24SykfyF/Y/0a7P3ckgg9MDY/bMkpWhmJt+LTJvC+7oP39dtIBBN1XwkggfGxsb4MeqzCv3q5xN8gXwBCoYsUTVYVAyKUwZrXmbLEqpBh4exEVXwYvj4AonjuhLCFbPLLq2H1V8ZRDxxEZNWSZLsrLqk1+gvura29EpPjuzGrvyCOCkVDOipAgt0xks+Qn5BmIzCr1yaJjp7JZwbXkMgKhDwRK9AvI5sOC86Uo6lJjZ5VBUgQmMMNE1iXfDVatNDjYlUyEduCBrqJ8eSa/kGc0dKKqssKR5zeemG099vUcyTyz4aLeTYmPWdBOF0PSikdQyJVkEg2wOTuK2SJZgUTmDEkL2MikRnylAxOYMbZLpbMN/PM/KgyTISTQxNAAoMlrUlvdycv4hLlycjAhoigTAyeyCzHJ8GCTopUEbHeb/ToZqBNaHAi9ExkyENmPPokiaXUTHdeGzJVRrR6xQRIe/v6CWhNV8MqFoGxuoxy0Uoc5nkdAQkjAX0I63lR5pbn87OXtqhVSxUgx1rXFfq5dA+YCiC0ycSmVoOectQLiCp9YmMdojSxrSSxR/IcpYiXj+6KSoFjxyqy/N3892AhBj8jXcYC5ETN/I1ZfDfnTi5vVlpZigHxeCrLmJ8J312vjHJKdQg8l/guK7y4qOTbZjpLn1KikCJA2twVyMTGh2SuUcJAr2eMDEhPp0rrcnPKImblGxYQj6cai3J+I2SUi4AryY2eZG0mdvFtQduJLMV5Ode3hmMTFhDhQcn+7ndgGYbIKBepngxvIX0KYGs5ycLOPyO7LGSm0tB7CK1VGe2MtmO8MEkwqGkspLf3onqy8gvyM0uHTCZDAoIx43VYxiWRWqVx7psLkJ4hhf01L6d0SN7gIYB4WitWYEPqUeNUthJJzAdIQCtON+Q6yjb013AAIG1tVVM5kYhbN3RGuaEQmRMQbFq5rcnJU6dlXfffPp0GAOJxV4oUTSYMvjcnIL1d17PoupYOAcTTuq6EcWmzkg7CgM/44WUZYQlHXdVwykfXFTjsHrSQNnflDnwxV0a5E+qbGxCiDZg03hAExOOp+C6T+bvq4DVGKdPMQ8JUl5zCJsINPhKwEIwdIiPbTcaoWlVSmNtChBvM2J15ztLHA4C0tVa2YRMpTVVVGKSQ2S0EHteOvJyy2dzj+dN5TJZFnnczX6a3EFH5EreN5e3uynthLvebGQ0YvA+Li7rt4carrjB7X8I97gq4urwkXkz0oTtCAIFhAJDKT1Fp0/WpuHhxGRmAwNI3CkDMny8LOJt9UBdNFadZ3h8hgIwMC4Gn9cX/AYlXL6qCLizkuADE9Ekw0bLamzzJViwEmWyVehBqRF7hZX0NL2ucCkCNU4TYwcZ2CzKrmjsrKdzeIxy7g5+ZZd88bAvAHk5jexKOJ5kkJjKcIthvByCVr2D9xFSpKIboQ7QFgCA+hP/AOGarQhKil3hba8WTOKke8tVBKkgmqshjX3gsVlj6bYkSQBu+9Chva6so5cSrtSGYGCoS59d/3mZJgaVXJUYCrbjSNbzDvfbUbmb5XCuSiaCDF29POPCN1Y7XzP4nEfy14hlYXBTEsPzeiOV3U6aKxTLDR+kZ5YGYxz1N1U0YR3QNFNIKDOixB/sheT2AuCvvxz/3akVcTzoICro9Pb38yV5AHgAgv9STv1a8MJdaif2Qh3sA6Tn+E3htm9kuO7OezDN6zsp+2lJ9uk/GizFNeA3YwhXyY8Yukpddbi5dqCIt48cDtp53N1Vvhbf1QzPpgQlh8ChQ8NRJe/u6C8kvRR3xk0DFiUmW6WlpZQMse3fz+osQ37gtgXJFy5os3DLtTMf1n4mCA08ummqSSGthHSHTZOxpWo+8i2aZ7FJVrrOsvA/FAYAcP74229dp2W/UKKl+Te+onfgUPmaZO1RzbGjZkM1leT+UM2S01wmZ6eio0amnnXbykraQgIgfsce+HC4YXjRp3AtvZbsyPf2mYRN2wkpExJehM8rBQ7wCgaFb+9d0yHAEnIB/CSfgrzAiJGgsqzHvGPD+23By7mlcDz2YUfV4CvOOIUs9oQN2qMLuaWNvw1spNBgo2+zpo+dyvqRLiVx7v66wd3Uki3SFhssoN9PhuIzzOb7BegwT0lY5HolZcF6Ln65E+Xg/g920f6al2+cAjJAvtQ/H/7MvK8d7uy1I82eQpXmi98Ymey/Nylp+PJTMwwZ9kvvZMe3krUX3lehMbFvt6d1Xcx5aiUiNYXfjs2OwziXeFZ9QPdDduux+/zWnnrrMG05mM4RFP5SWUa7JcggGejHIJyq8+75cZymWdoa/FAEiSHhaK5fh4ym9XGK0JjeXeGla2rK6SEpEcx8LkOVIGrNKL5dYbMtaLNJ1M7KXvqFETsWACGK9qTVEeryFSoirfobozzyJr7Dby7Hfr/21t+n5rG7WBT14HPXAexowfbCdZLln6tilHqVaRAVIH9Fj7gokLhOrqoRMbFyjgH3yoTVtlJL4gwDiE6UKxPIcurDZ0EF0h9rpgZMjsMD1yMnzMBIERL3PpAqQvko4frx6cne3fzGXaTEG/gvVVA68pzcgxPNM8rnS0pYHgx/V0FJbZl9z9eQumV2F1xQshixq0xbWIar2BdsoS200FjFY5pgA6U/M41kzjsnJJVjYy+1JXEaToCCysFEm9uxHYUz4Akv8Lbh3SMIfDLrBnuHbDM8puGygtkK1LLfv0HPjuv3dJSTzXDSWLDS0iTjjORmVjYTS0I7RQSRla8FYehi/t6B/+CCD2V52OJaI820xX/8DHAGVxL5fMxsAAAAASUVORK5CYII="
        />
        <meta property="og:url" content={currentUrl} />
      </Helmet>

      {isLogin !== undefined && isLogin ? (
        <Switch>
          <Layout path="/subscription/checkout" component={<SubscriptionCheckout />} />
          <LayoutLokalac path="/subscription/checkout-rs" component={<SubscriptionCheckoutLokalac />} />
          <Layout path="/subscription/shop/registration" component={<ShopRegistration />} />
          <LayoutLokalac path="/subscription/shop/registration-rs" component={<ShopRegistrationLokalac />} />
          <Layout path="/payment" component={<Payment />} />
          <Layout path="/sei-dabei" component={<Packages />} />
          <LayoutLokalac path="/sei-dabei-rs" component={<PackagesLokalac />} />
          <Layout path="/dashboard" component={<Download />} />
          <Layout path="/dashboard-rs" component={<DownloadLokalac />} />
          {shopOwner ? <Redirect to="/dashboard" /> : <Redirect to="/subscription/shop/registration" />}
        </Switch>
      ) : (
        <Switch>
          <Layout path={['/registration', '/follower', '/sei-dabei/ambassador']} component={<Registration />} />
          <LayoutLokalac
            path={['/new/registration', '/rs/follower', '/rs/voucher']}
            component={<RegistrationLokalac />}
            hideHeaderMenuItems
          />
          <Layout path="/sei-dabei" component={<Packages />} />
          <LayoutLokalac path="/sei-dabei-rs" component={<PackagesLokalac />} />
          <Layout path="/login" component={<Login />} />
          <LayoutLokalac path="/login-rs" component={<LoginLokalac />} />
          <Layout path="/download" component={<Download />} />
          <LayoutLokalac path="/download-rs" component={<DownloadLokalac />} />
          <LayoutLokalac path="/download-rs-registration" component={<DownloadLokalacRegistration />} />
          <LayoutLokalac path="/terms-rs" component={<TermsLokalac />} />
          <LayoutLokalac path="/terms-bih" component={<TermsLokalacBih />} />
          {currentUrl.includes('lokalac') ? <Redirect to="/download-rs" /> : <Redirect to="/sei-dabei" />}
        </Switch>
      )}
    </>
  );
};

export default App;
