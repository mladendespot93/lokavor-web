import { IShopRegistration } from './shop';

export interface ISubscriptionStore {
  currency: string;
  shopData: IShopRegistration | undefined;
  status: 'idle' | 'loading' | 'success' | 'error';
  activeSideAnimation: boolean;
  clientSecret?: string;
  clientSecretStatus: 'idle' | 'loading' | 'success' | 'error';
  shopId?: number;
}
