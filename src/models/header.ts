export interface INavItems {
  key: number;
  name: string;
  url: string;
}

export interface IHeader {
  index: number;
  name: string;
  navItems?: INavItems[] | INavItems;
}

export interface IHeaderComponent {
  navData: IHeader[];
}
