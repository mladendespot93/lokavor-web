export interface IUserRegistration {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  matchingPassword: string;
  searchParameters?: string;
  country?: string;
  terms?: boolean;
}
