import { AxiosResponse } from 'axios';

export interface IUserLogin {
  email: string;
  password: string;
}

export interface TokenObject {
  access_token: string;
  refresh_token: string;
}

export interface IAuthStore extends TokenObject, IUserInfo {
  loginError: boolean;
  isLogin: boolean | undefined;
}

export interface IUserInfo {
  sub?: string;
  email_verified?: boolean;
  shopOwner?: boolean;
  name?: string;
  shopId?: number;
  preferred_username?: string;
  given_name?: string;
  family_name?: string;
  email?: string;
  stripeAccountId?: boolean;
  shopToFollowId?: string;
}

export interface UserInfoResponse extends AxiosResponse {
  data: IUserInfo;
}
