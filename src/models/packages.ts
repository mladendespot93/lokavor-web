export interface IPackages {
  activePackages: Array<{
    key: number;
    title: string;
    titleImage: JSX.Element;
    subTitle: string;
    list: Array<string>;
    price: string;
    discountPrice: string;
    styles: {
      backgroundImage: string;
    };
  }>;
  prices: {
    discountPrice: string;
    totalPrice: string;
    mounthPrice: string;
  };
  paymentIntent: string;
}
