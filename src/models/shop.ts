import { AxiosResponse } from 'axios';

export interface IShopRegistrationForm {
  name: string;
  address: string;
  city: string;
  postalcode: string;
  phone: string;
  website: string;
  primaryShopType: string;
  secondaryShopTypes: string;
}

export interface IShopRegistration {
  name: string;
  phone: string;
  website: string;
  primaryShopType: string;
  secondaryShopTypes: string[];
  addressDto: IAddressDto;
  openHoursDto: IOpenHoursDto[];
  registeredType?: string;
}

export interface ShopTypesResponse extends AxiosResponse {
  data: IShopType[];
}

// SHOP TYPE MODEL
export interface IShopType {
  id: number;
  name: string;
  label?: string;
}

export interface IShopStore {
  shopTypes: IShopType[];
  shopRegistration: {
    openHoursDto: IOpenHoursDto[];
    primaryShopType: string;
    secondaryShopTypes: string[];
  };
}

export interface IOpenHoursDto {
  dayOfWeek?: string;
  from?: string;
  to?: string;
  breakFrom?: string;
  breakTo?: string;
  name?: string;
  nonWorkingDay?: boolean;
}

export interface IAddressDto {
  street: string;
  city: string;
  country: string;
  postalCode: string;
  latitude: number;
  longitude: number;
  countryCode: string;
  locale: string;
}

export interface ISubscription {
  lokavorProductPriceId: string;
  shopId: number;
  shopName?: string;
}

export interface IFileRequestDto {
  id?: number;
  multipart?: string;
  url?: string;
  logoImage?: boolean;
  operation?: string;
  path?: string;
}

export interface ICertificate {
  id: number;
  name: string;
  link: string;
  issuer?: string;
  imageUrl?: string;
}

export interface IShop {
  id: number;
  name: string;
  ownerId: string;
  phone?: string;
  website?: string;
  certificates: ICertificate[];
  addressDto: IAddressDto;
  openHoursDto?: IOpenHoursDto[];
  primaryShopType: string;
  secondaryShopTypes?: Array<string>;
  registeredType: string;
  defaultCurrency: string;
  images: IFileRequestDto[];
  distance?: number;
  logoImage?: IFileRequestDto;
  history?: string;
  philosophy?: string;
  shopsList: IShop[];
}
