import lokavorLogo from './header/logo.png';
import lokalacLogo from './header/lokalac@3x.png';
import lokalacLogoSmall from './header/lokalacLogo.png';
import lokalacLogoNav from './header/lokalacLogoNav.png';
import menuIcon from './header/menu.svg';
import spinnerLoader from './button/spinnerLoader.svg';
import micrositePackage from './packages/microsite.png';
import quickShopPackage from './packages/quickshop.png';
import quickShopBadgePackage from './packages/quickshop_badge.png';
import micrositeBadgePackage from './packages/microsite_badge.png';
import discountBadge from './packages/discount_badge.png';
import trashDelete from './packages/delete.svg';
import dashboardGroupImage from './dashboard/dashboard_group_image.png';
import dashboardGroupImageLokalac from './dashboard/dashboard_group_image_lokalac.png';
import appleStoreBadge from './dashboard/apple-store.png';
import googlePlayBadge from './dashboard/google-play.png';
import realBackground from './dashboard/real_background.png';

import loginBackgroundImage from './main/LoginBackground.png';
import shopRegistrationBackgroundImage from './main/ShopRegistrationBackground.png';
import registrationBackgroundImage from './main/RegistrationBackground.png';
import loginGroupImages from './main/Login.png';
import registrationGroupImages from './main/Registration.png';

export {
  lokavorLogo,
  menuIcon,
  spinnerLoader,
  micrositePackage,
  quickShopPackage,
  quickShopBadgePackage,
  micrositeBadgePackage,
  discountBadge,
  trashDelete,
  dashboardGroupImage,
  dashboardGroupImageLokalac,
  appleStoreBadge,
  googlePlayBadge,
  realBackground,
  loginBackgroundImage,
  shopRegistrationBackgroundImage,
  loginGroupImages,
  registrationGroupImages,
  registrationBackgroundImage,
  lokalacLogo,
  lokalacLogoSmall,
  lokalacLogoNav,
};
