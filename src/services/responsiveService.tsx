interface Viewport {
  height: number;
  width: number;
}

const mobileViewport: Viewport = {
  height: 640,
  width: 360,
};

const desktopViewport: Viewport = {
  height: 1080,
  width: 1920,
};

// get initial screen's height
export const screenHeight = window.innerHeight;

// get initial screen's width
export const screenWidth = window.innerWidth;

// calculate the ratio of the device viewport the view is rendered on
// compared to the initial design viewport
const responsiveMobileRatio: Viewport = {
  height: screenHeight / mobileViewport.height,
  width: screenWidth / mobileViewport.width,
};

const responsiveDesktopRatio: Viewport = {
  height: screenHeight / desktopViewport.height,
  width: screenWidth / desktopViewport.width,
};

const responsiveToHeight = (pixels: number, viewport: string): number => {
  return viewport === 'mobile' ? pixels * responsiveMobileRatio.height : pixels * responsiveDesktopRatio.height;
};

const responsiveToWidth = (pixels: number, viewport: string): number => {
  return viewport === 'mobile' ? pixels * responsiveMobileRatio.width : pixels * responsiveDesktopRatio.width;
};

export { responsiveToHeight as rh, responsiveToWidth as rw };
