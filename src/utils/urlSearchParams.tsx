export function urlSearchParameters(searchParameters: string | undefined): URLSearchParams {
  return new URLSearchParams(searchParameters);
}
